---------------------------

===========
Kommentarer
===========

Jeg har fått noen kommentarer på denne teksten (versjonen som er datert 2019-06-10 13:28:47):

---------------------------

.. topic:: Erik skriver (2019-06-10 23:38):

    Hei Pål, da har jeg finlest dette innlegget et par ganger og har noen refleksjoner.

    Først og fremst er dette viktige og interessante ideer som fortjener oppmerksomhet.

    Det er dog et par grunnleggende problemer med framstillingen som lett kan ødelegge
    for mulighetene til å nå fram til flere enn noen få.

    For meg er det essensielt å skille mellom problem og løsning. Alle de problemene du
    nevner er reelle, men du kan ikke uten videre ta alt dette til inntekt for "gaveløsningen".
    Du hopper litt for rett på, for. meg.

    Det er greit at du har et par kildehenvisninger, men jeg savner mer vitenskapelig bakgrunn.
    Jeg blir sittende igjen med et inntrykk av at dette er Påls ønsketenking, fjernt fra realisme
    og nøkternhet i forhold til den kompleksiteten som ligger i overgangen til et nytt system.

    Er det gjennomtenkt når du først sier at det er et problem at ytelser fra det offentlige er upersonlige,
    for så å uten videre introdusere borgerlønn som en del av "gavepakken"?

---------------------------

.. topic:: Arne skriver (2019-06-11 20:16):

    Hei Pål!

    Du tenker så det knaker, men når jeg skal prøve å følge deg inn i ditt hyperinflasjonssamfunn
    så strekker ikke min fantasi til. Hvordan skaffer man seg en leilighet i et slikt samfunn?

    Når det gjelder gavepenger anser jeg gjengjeldelsesgaver som noe gammeldags. Min gamle stemor
    tør ikke ta i mot noe fra naboene fordi hun ikke kan gi noe tilbake. Jeg hjelper selvsagt dem som hjelper meg,
    men føler meg ikke bundet. Jeg bodde en gang på hybel hos en gammel mann som sa at det du har fått skal du gi videre.
    Min moral er hvis jeg får mye så gir jeg videre til dem som ikke har i stedet for å gi til dem som ga til meg.
    Det moderne prinsipp er frie gaver, altså gaver som gjør folk frie og ikke binder til gjengjeldelse.

    Du skriver mye klokt om borgerlønn og det er en interessant diskusjon om hva som er en gave og hva som er en rettighet.
    En forutsetning for at et borgerlønnsamfunn skal utvikle seg er at vi slutter å tro at vi arbeider for å få lønn,
    men at vi innser at vi arbeider for å løse meningsfulle oppgaver. Det er en av de store utfordringene i vår tid å få flere
    til å innse at dette er en selvfølgelighet.

    Når det gjelder det at penger må dø så er Rudolf Steiner inne på dette. Jeg skal gjøre en ny studie av hans foredragsrekke
    som kalles Samfundsøkonomi på dansk, i håp om å forstå det bedre.
    Men Steiner skildrer mellom kjøpepenger, lånepenger og gavepenger. Jeg har laget en forenklet versjon av dette i min bok.
    Kjøpepenger bruker vi i vår daglige handel. Lånepenger (Sparepenger) gir grunnlag for investeringer,
    men når investeringer har skapt overskudd så må disse pengen dø og ikke gå videre i spekulativ virksomhet.
    Måten de dør på er at de går tilbake til å bli gavepenger. Penger som kan stimulere utviklingen av et fritt kulturliv
    som blant annet kan inspirere oss til å forstå at vi faktisk arbeider for å løse oppgaver.

---------------------------

.. topic:: Mine svar til Erik og Arne (2019-06-13 21:45):

    Dette synes jeg var nyttige kommentarer. Jeg ser nå at endel momenter må endres, og at teksten ville blitt tydeligere
    dersom jeg inkluderte noen flere referanser.
    Jeg ble f. eks. inspirert til å skrive om 'svart svane' når jeg leste Equinors
    `Energy Perspectives 2019
    <https://www.equinor.com/en/how-and-why/energy-perspectives.html>`_.
    Der omtaler de svarte svaner. Disse er rimelig fysiske av natur, mens jeg tenker (ønsketenker...) at en svart svane
    like gjerne kan være en ny politisk/sosial idé.
    Avveiingen mht. referanser blir hvor lang en slik tekst skal være i forhold til hvor
    grundig den blir.

    Det slår meg at seksjonen med hyperinflasjon bør endres sterkt.
    Grunnen til det er at dette er kun er en teoretisk betraktning innenfor en veldig enkel modell som er så overforenklet
    at den aldri kan ha noe med "virkeligheten" å gjøre.
    Momentet med at det må skapes nye penger, flere enn de som blir 'ødelagt', er det som er vesentlig her.
    Det er flere 'skoler' som er inne på dette, f. eks. Positive Penger (Danmark).
    Varianter av dette finnes også, men ikke alltid som inflasjon. Poenget er at penger må miste verdi med tiden.
    Silvio Gesell foreslo (og det er testet ut) et system med frimerker som måtte klistres på pengesedlene regelmessig.
    Charles Eisenstein har videreført Gesells tanker i form av et pengesystem uten kontanter men med negativ rente.
    I de to siste systemene kan pengemengden være konstant og de blir likevel mindre verdt med tiden.

    De 7 punktene jeg har i listen over fordeler er selvfølgelig tildels overdrevet, og er på ingen måte sikker på at
    det er dekning for alle. Sånn sett har Erik helt rett i at dette er min ønsketenkning!
    Jeg prøver vel heller ikke å legge skjul på at dette er tildels basert på ønsketenkning, jeg bruker vendinger som
    "Borgerlønn kan assosieres med gaveøkonomi".
    Jeg er litt usikker selv på om dette holder i en grundigere analyse.
    Enkelte resultater av forsøk med borgerlønn har vel vist at mottak av en fast penge-ytelse kan sette igang positive prosesser.
    Jeg skriver også om indoktrinering. Den alminnelige måten å se samfunnet på, f. eks. at vi bare må ha vekst,
    kan ikke rokkes ved uten at mange nok får motforestillinger mot det, og det er jo mer eller mindre ønksetenkning
    at noe vesentlig skal skje her (men du verden så nødvendig).

    Både Arne og Erik trekker fram hva gaveøkonomi egentlig er, og om det kan kobles til borgerlønn.
    Arne sier at gaveøkonomi også har en negativ side ved seg, nemlig forventningen om gjenytelse.
    Helt enig, det har slått meg også i det lille jeg har lest om gaveøkonomi, så kanksje det det å trekke inn
    gaveøkonomi er et feilgrep fra min side?
    På sitt beste er jo gaveøkonomi bra, da. Borgerlønn på sitt beste kan jo komme til å fungere bra,
    i og med at folk allerede nå yter ganske mye for felleskapet uten betaling (i frivillige organisasjoner ol.)

    Når det gjelder Steinar Rudolfs tanker om penger som Arne skriver om, så er dette noe jeg må lese mer om.
    Jeg hopper jo også glatt over slike ting som banklån og investeringer i min tekst.
    Dette er jo naturligvis noe som må med i en grundigere tekst, men poenget mitt var jo å koble sammen
    gaveøkonomi, borgerlønn, KAF, nullvekst, negativ vekst uten katastrofale følger vha. "kunstig" inflasjon
    i en ikke altfor lang tekst.
    Mulig jeg lyktes bedre i mitt eget hode en enhver annens hode, jeg vet ikke jeg...

---------------------------

.. topic:: Andrea skriver (2019-06-21 21:35):

Hei Pål, nu har jeg endelig læst din blogpost!

Gode overvejelser du har, synes jeg!
Og jeg synes altid det er fedt og kontruktivt med diskussion og overvejelser rundt potentielle nye organiseringer af pengesystemet.

Her er alligevel et par kritiske kommentarer:

1. Jeg tror netop, at det at gavemodtageren føler at den skylde noget tilgengæld er det der gør,
   at det er hårdt at være på overførselsindkomst længe, og som bidrager til stigmatiseringen
   af folk på langtidsoverførselsindkomst!
   Modtageren føler at man ikke kan give samfundet det tilbage som man burde.
   Især når der oveniløbet er regler om at man ikke **har lov** at bidrage med den smule man
   kan - f.eks arbejde et par timer om ugen.
2. Jeg tror egentlig ikke man kan kategorisere nogle samfund som "gaveøkonomier" og andre som "pengeøkonomier".
   Jeg tror alle økonomier/samfund indeholder elementer af begge.
   F.eks er familierelationer oftest bygget op om gaverelationer,
   mens arbejdstager/giver-relationer er baseret på penge.
   De to typer relationer fungerer sideløbende i det samme samfund/den samme økonomi.
3. Jeg synes også der er noget der bliver lidt fejl ved at lave skillet baseret på rækkefølgen i ydelsen, - for
   det kommer jo an på hvis perspektiv man ser det fra.
   Jeg "giver" jo min arbejdskraft til min arbejdsplads **før** jeg får løn.
   Omvendt **kræver** jeg mad, husly og omsorg af min mor **før** jeg begynder at hjælpe hende med at reparere huset,
   når hun bliver ældre og jeg bliver voksen.

Har du læst David Graeber *Debt - the first 5000 years*?
Den er virklig god og handler akkurat om dette.

Bortset fra det er jeg helt for borgerlønn!!
Jeg har nu ikke tænkt på om det ha inflydelse på vækstpresset i økonomien - spændende tanke.
Måske især pga det lavere gældpres på individet?

Positive Money og Ole Bjerg har forsøgt at argumentere for at det nuværende system
hvor penge er *rentebærende* gæld skaber vækstpresset.

Vi kan snakke mere om dette senere!

---------------------------

.. topic:: Mitt svar til Andrea (2019-06-21 23:00):

Takk Andrea!

Det du skriver om stigmatisering har jeg også fundert over.
Dette blir litt diskusjonen om hva som er best av Job Guarantee (JG) eller Borgerlønn (UBI).
JG-forkjemperene mener det du uttrykker, at UBI kan skyve mennesker ut av felleskapet.
Jeg tror at man gjerne kan tenke seg at man både har UBI og JG samtidig,
men jeg tror det er enda viktigere at man
revurderer hva som skal være full arbeidsuke med jevne mellomrom.
Det er ingen naturlov som sier at 100% stilling = 40 timer pr. uke.
"Full stilling" er et tøyelig begrep, og må tilpasses etter hvilken grad av automatisering man velger.

Men det du sier om å **ha lov** til å arbeide ser jeg ikke som noen problemstilling.
Det er jo nettopp styrken ved borgerlønn (UBI) at den gis uten behovsprøving,
man får like mye uansett hvor mye eller lite man arbeider, og lønn for arbeid kommer alltid i tillegg til borgerlønnen.

Jeg er enig i at alle samfunn består av gaveøkonomi til en viss grad.
Poenget mitt er at man vanligvis ser på det som tvingende nødvendig at kun en brøkdel
av økonomien **kan** være gaveøkonomi.
Jeg prøver å argumentere for at det går fint å tenke seg at store deler av økonomien er bygd opp etter
gaveøkonomi-prinsippet.

Du har et godt poeng når du sier at det kommer an på perspektivet man ser ting (ytelse/lønn),
i alle fall dersom man tenker analogt med at penger og gjeld er det samme sett fra hver sin side,
og jeg trenger antakeligvis å lese *Debt - the first 5000 years*!

Positive Money/Ole Bjergs betraktinger om *rentebærende gjeld* er indirekte inkludert i min tekst.
Jeg sier jo at Borgerlønn kan være penger som oppstår fra ingenting.
Det jeg ikke sier, (men som er underforstått), er at når man skaper penger,
må man regnskapsmessig samtidig føre en motpost.
Dette **er** gjeldsposten, men man kan definere at denne gjelden er rentefri og evigvarende, og at det er
"samfunnet" som er skyldneren (slik som Positive Money også gjør).
Dersom man (av en eller annen merkelig grunn) ved lov forbyr at en slik gjeldspost kan være
rentefri og evigvarende, da er man plutselig i samme uføre som vi nå har!

---------------------------

.. topic:: Andrea skriver (2019-06-25 15:38):

Hei igen, et svar til dit svar:

JG vs. UBI:

Ja en kombination af de to kunne sikkert være smart på mange måder.
Det vigtige er vel at også dem der faktisk ikke er i stand til at arbejde også har råd til at leve.
Og at antallet (ugentlige) arbejdstimer er fleksibelt for hver enkelt person uansett løsning,
som du også skriver.

Rentebærende gjeld:

Det kan vel være kontruktivt at tænke på hele *tre* forskellige mulige typer penge
(som vel sånn set alle opstår ut af en form for "ingenting" - i modsætning til varepenge (fx guld)):

1. Gæld, der må betales tilbage OG som er rentebærende
   (fx de nuværende elektroniske penge som skabes af private banker) - det et disse som
   Ole Bjerg og Positive Money foreslår er med til at opretteholde vækstpresset i økonomien,
   for hvad skal man betale de ekstra renter med,
   ud over de der opstod ved lånet? - Et nyt lån selvfølgelig! Og så videre.....!
2. Gæld med uendelig løbetid, som ikke er rentebærende
   (fx kontanter, som skabes af staten)
3. Penge som hverken er gæld eller rentebærende
   (det er vel her en længere diskussion om man mener statens penge teknisk set er gæld eller ikke,
   hvis den aldrig må betales tilbage,
   og der ikke er en specifik person som har optaget gælden,
   men pengene snarere er *brugt* ind i økonomien - Gode Penge/Positive Moneys
   forslag til et nyt Sovereign Money system.
   På den anden side kan man vel som MMT'ere nok vil si,
   at "gælden" betales løbende tilbage i form af skatter og afgifter,
   hvorfor man netop ikke blir nødt til at devaluere den kontinerligt stigende pengemængde hele tiden).

---------------------------

.. raw:: html

    <a name="C2019-06-30-19-00"></a>

.. topic:: Mitt svar til Andrea (2019-06-30 19:00):

Det du skriver om Ole Bjergs oppfattelse av vekst-tvang med begrunnelse i renter på lån er interessant.
Han var jo innleder på Rethinking Economics sitt seminar,
`Rethinking Sustainability 9. februar 2019
<https://www.facebook.com/events/308400183220530/>`_.
I innledningen sa han selv at 'bevisene' for vekst-tvang på bakgrunn av rente-betraktninger ikke er enkle å legge fram på
en vitenskapelig måte.
(Det er mulig at innledningen finnes på video, jeg finner muligens tilbake til hans formulering senere...)

Dette er det jeg oppfatter som vanskeligheten med bevis-førselen:
Problemet med å betale "de ekstra renter" er ikke så stort som man først skulle tro,
fordi penger sirkulerer.
I den tradisjonelle fortellingen sies det jo at de ekstra rentene som kreves inn ikke eksisterer,
fordi de ikke er en del av den opprinnelige pengemengden.
Men i og med at penger sirkulerer, så kan pengeinnkreveren bruke de innkrevde pengene tilbake i økonomien som en konsekvens
av at pengeinnkreveren også er en aktør i økonomien.
Slik kan "den samme pengeseddelen" inngå i innbetaling av renter flere ganger, siden ikke alle rente-pengene og avdragene
forlanges tilbake-betalt samtidig.
Altså, skyldneren får 'tilfeldigvis' hånd om de samme pengesedlene med noen måneders mellomrom pga. aktiviteten i økonomien.

Til tross for dette resonnementet som kan tyde på at det ikke i det hele tatt finnes noe problem ved vårt nåværende pengesystem,
så tror jeg denne problemstillingen har en viss relevans.
Det er jo uten tvil slik at alle penger vil forsvinne dersom alle prøvde å betale tilbake lånene sine.
Det oppstår problemer dersom økonomien går tregt slik at ingen ønsker å ta opp lån,
fordi man da er på grensen til at pengemengden er synkende, hvilket gir alle typer økonomiske problemer, bl. a. problemer med
å 'finne' penger som kan brukes til å betale tilbake lån.

Så vårt nåværende system er mye enklere å håndtere dersom vi har økonomisk vekst, en vekst som (etter min mening)
ikke kan fortsette evig. Noen økonomer 'løser' dette problemet ved å si, 'Joda, vi kan ha evig vekst', og vip(p)s,
så trenger han ikke bry seg om at systemet blir ustabilt når vi ikke har vekst.

Forøvrig tror jeg det er viktig å være klar over at det ikke er kun feil ved pengesystemet som gir oss tvangen, eller lysten,
til å vokse.
Det er andre faktorer også,
som f. eks. lysten til å løse en arbeidsoppgave mer effektivt,
nysgjerrighet over hvordan naturfenomener henger sammen,
ønket om å leve et behagelig liv, osv.

Problemet er (slik jeg ser det), at det kan jo tenkes at de fleste behagelighets-problemer blir bortimot løst,
og at det da er om å gjøre å kunne la systemene vi omgir oss med fungere i en tilstand der vi ikke *ønsker*
mer vekst av det kvantitative slaget.
På dette punktet feiler systemene være nå, blant andre ting, også pengesystemet.


Når det gjelder alle de tre punktene dine samlet sett, så har jeg et svar her:
`Folkekapital
<http://saugstad.net/folkekapital>`_
er et ment som et konsept som forbedrer pengesystemet i samme retning som forslaget til
`Positive Money
<https://positivemoney.org/>`_.
Med Folkekapital vil ikke bankene bli fratatt all rett til å skape penger (slik som Positive Money vil),
men de vil bli pålagt å utstede evigvarende rentefrie penger vha. obligasjoner utstedt av en offentlig kontrollert organisasjon.
Funksjonen til denne organisasjonen blir antakelig på linje med Positve Money sin komité som skal bestemme nivået på pengemengden.
I Folkekapital-forslaget har jeg ikke det skarpe skillet mellom pengeskaping og pengeformidling slik som Positive Money vil ha.
Jeg tror det er mer realistisk å få gjennomslag for et Folkekapital enn for forslaget til Positive Money,
og det de har felles er evnen til å kunne fungere i et klima der vi har nullvekst eller svak negativ vekst.
