.. raw:: html

    <figure>
        <img src="img/bregner.jpg" width="700" height="447" alt="Bregner">
    </figure>

Det gir sosial status å gi gaver.
Mottakeren av en gave føler ofte at hen står i en slags moralsk gjeld til gavegiveren.
Det kan utløse en kjede av gaveoverrekkelser.
Noen steder har dette vært en utbredt måte å organisere hele samfunn på.
Gavene vil i så fall ikke bare være symbolske kjekt-og-ha gjenstander,
men nyttige ting som mat og bygging av hus og andre livs-nødvendigheter.

I vårt velferds-samfunn har vi noe av det samme, bortsett fra at gavene er upersonlige.
Jeg tenker da på trygdeytelser, pensjoner, og gratis skole- og helsetjenester.
Dette er vedtatte "gaver" som blir gitt i form av penger eller tjenester.
"Giveren" er fellesskapet via skatter og avgifter.
I og med at det ikke er noen direkte kobling mellom en personlig giver og en personlig mottaker,
så oppleves denne måten å redistribuere velstand på på en helt annen måte.

Giverene (skattebetalerene) føler ingen særlig glede ved giverdelen av transaksjonen.
Mottakeren føler ofte ikke at de står i gjeld til noen,
for den konkrete giveren er et offentlig kontor.
Ofte, (og med rette), opplever mottakeren at ytelsen er en rettighet, ikke en gave.

Et ytterligere problem med denne modellen, er at den tilsynelatende ikke er bærekraftig.
Utallige rapporter og analyser er laget som viser at velferdsytelsene kommer til å krympe
framover. Kron-eksempelet er eldrebølgen, som kommer til å overskylle hele verden,
i alle fall den delen av verden med sterkest økonomi.

Vi trenger en holdningsendring når det gjelder å forstå hvordan samfunnet fungerer,
og hva vi kan gjøre for å få det til å fungerer bedre.
Vi trenger en re-indoktrinering.

En grunntanke i vårt samfunn er at du må yte noe (arbeide) og hvis du er dyktig, så får du lønn.
Det anses for å være på sin plass at lønnen minst motsvarer det arbeidet man har utført.
Man vil ha så stor lønn som mulig slik at man kan skaffe seg minst sin andel av samfunnskaken, helst mer.

Dette er stikk motsatt logikk av det som er grunntanken i et gavesamfunn.
Der er det slik at man først mottar "lønn", og deretter føler man seg forpliktet til å gi en gjenytelse.
Det gir mer status å gi en minst like stor gjenytelse som den man mottok.
Grunnen til at dette kan fungere er at man i sin tur senere kan forvente at egen ytelse blir gjengjeldt med en
enda større ytelse.

Begge disse måtene å tenke på gir opphav til vekst. I første tilfelle kan vi si at vekstimpulsen består av krav,
i utgangspunktet en negativ eller usympatisk holdning, mens i siste tilfellet består vekstimpulsen av
ønsket om å gjøre seg nyttig, en positiv eller sympatisk holdning.

Jeg tror dette er mye av grunnen til dannelsen av begrepet "The Economic Man".
Han er en egositisk type med full informasjon over markedet som alltid opererer for å maksimalisere sitt materielle utbytte.
I en gaveøkonomi vil prototypen på en aktør heller være en altruistisk person som i stor grad tenker på og tar
hensyn til sine medmennesker.
På mange måter er vel dette en bedre beskrivelse av gjennomsnittsmennesket?

Det er nok nå
-------------

I vår nåværende øknonomi er det en konstant diskusjon om hva som driver veksten videre.
De fleste analysene av klimakrisen tegner et framtidsscenario der
fortsatt økonomisk vekst er et viktig kriterium.
Hvorfor det?
Det viser seg jo faktisk at veksten er svært lav i de sterkeste økonomiene.
Det i seg selv sees på som en litt uforståelig tendens, hvorfor vil ikke økonomien vokse slik som den alltid har gjort?
Etterspørsels-svikt er en nærliggende forklaring.
I så fall, så er det vel ingen grunn til å vokse enda mer?
Eneste grunn måtte være at alle systemene våre, bankvesenet og børsen ikke er bygget for tilbakegang.
Tilbakegang er pr. definisjon krise.
Tilbakegang gir en kaskade av problemer i form av arbeidsløshet, betalingsvansker, konkurser osv.
Det er så innarbeidet at man blir sett på med måpende blikk hvis man overhodet tillater seg å spørre om
det kan være noen mening med livet dersom økonomien ikke vokser.
Jeg vil påstå at mange totalt indoktrinert på dette området,
og at denne indoktrineringen er ett av vår tids aller største problem!

Hvordan forholder alt dette seg i en gaveøkonomi? Vil den altruistiske aktøren forlange vekst?
Jeg synes det er enklere å tenke seg at veksten i en gaveøkonomi kan bremse opp nokså problemfritt.
Vekstimpulsen blir jo her "hemmet" av at behovet for gaver blir mindre, ikke at lysten til å gi gaver blir mindre.

Kan man tenke seg et stort, komplisert samfunn som bygger på prinsippene i gaveøkonomien?
Jeg tror absolutt det!
Jeg tror faktisk det er en forutsetning for å kunne takle klimakrisen samtidig
som ulikheten i samfunnet kan holdes på et lavt nivå.
men det er endel historiefortelling som må til for at det skal fungere.
Vi må rett og slett indoktrineres på en ny og bedre måte.
Så derfor vil jeg gjerne starte indoktineringen her og nå.
Hvis du er redd for indoktrinering må du stoppe å lese umiddelbart!

Dvs. indoktrineringen kommer litt lenger ned.
Først en grov skisse av et samfunn der gaveøkonomi er et sentralt element.

Borgerlønn
----------

`Borgerlønn
<https://no.wikipedia.org/wiki/Borgerl%C3%B8nn>`_
er et viktig element i modellen. Borgerlønn kan assosieres med gaveøkonomi.
I stedet for fysiske gaver, er her penger den initielle gaven.

Poenget med borgerlønn slik jeg tenker meg det, er først og fremst å bryte noen ustabile tilbakekoblinger
og erstatte dem med et vesentlig enklere system som er stabilt.
Et viktig element i dette er å forstå hva penger er, hva vi kan og ikke kan gjøre med dem, og hvilke egenskaper vi ønsker at penger skal ha.
Og dette er svært viktig: Penger er ikke et naturgitt fenomen, vi finner ingen mønstere i naturen som ligner det aller minste på penger.
Dermed kan vi definere hva vi ønsker at det skal være akkurat slik vi vil.
Men, vi må være enige i definisjonen, i alle fall sånn omtrent enige, ellers blir det kaos.
Mye av problemet med penger er at man har tenkt seg at de har et element av natur i seg.
F. eks. at penger er en vare på lik linje med andre varer, og at de har en pris.
Dette er en veldig vanlig tenkemåte, men veldig feil.
Det finnes mange som har advart sterkt mot denne måten å tenke på, bl. a. Karl Polanyi i boken "The Great Transformation/Den liberale utopi".

Slik man ofte tenker på samfunnet inklusive penger nå, så er det en slags stor maskin der pengene går rundt og rundt for å smøre maskinen,
slik at man kan ekstrahere ressurser ut av jord og luft, omdanne det til gjenstander, bruke dem en stund, og så dumpe dem.

Det vi trenger er å tenke helt motsatt: Penger er lette å skape, og lette å dumpe, og kan godt skapes ett sted i systemet,
gå lineært gjennom systemet motsatt vei av varer og tjenester, for så å bli dumpet et annet sted i systemet.
De fysiske ressursene bør i størst mulig grad bevares, og når de er utslitte,
bør de inngå i et sirkulært system slik at de gjenoppstår som nye produkter.

Borgerlønn *er* (slik jeg tenker på den) skapelsen av nye penger. Borgerlønn trenger ikke å bli "finansiert",
fordi borgerlønn er en omfordelings-mekanisme.

Uten en mekanisme for å fjerne penger, vil penger oppstå når de blir utbetalt til hver enkelt, og ha evig liv etter dette.
La oss si at samlet borgerlønn-utbetaling må være 10% av eksisterende pengemengde pr. måned.
Hver måned måtte i så fall utbetalingene vært 10% høyere enn måneden før. Det gir jo en ganske formidabel inflasjon.
10% pr. måned blir noe over en 3-dobling av pengemengden pr. år.
Prisen på varer og tjenester må øke omtrent like mye.
Hyperinflasjon, altså.

Men, det ville funket! Det er ingenting som må betales tilbake eller noe slikt,
og selvom kurven for pengemenge-utviklingen er eksponensiell,
så er ikke det noen krise i seg selv, siden penger ikke er noe fysisk,
det er bare en regne-enhet.
Av og til hadde man trengt å redefinere basisen for penge-enheten for at antall siffere i beløpene ikke skulle bli for store.
Mao., hatt overganger som "1000 gamle-kroner = 1 ny-krone" sånn over natten i ny og ne.
(Dette er jo noe ethvert pengesystem der man optimaliserer på en positiv stabil inflasjon trenger av og til.)

Avgifter
--------

En mekanisme for å fjerne penger kan likevel være gunstig.
En kan tenke seg at skatter og avgifter er slike mekanismer.
For å gjøre det enkelt, la oss tenke oss bare én avgiftstype, energi-avgift.
Er det noe som kjennetenger samfunnet, så er det at økt energiforbruk øker velstanden.
Høyt energiforbruk har dessverre også mange uønskede bi-effekter, som ødeleggelse av natur og utslipp av klimagasser.
En generell energi-avgift kan altså være nokså sikker og stabil kilde til pengedestruksjon.

Her vil jeg nevne
`Karbonskatt til fordeling (KAF)
<https://naturvernforbundet.no/klima/hva-er-karbonavgift-til-fordeling-kaf-article34560-126.html>`_
som er foreslått fra flere hold.
Det er en ordning der man tenker seg at innbetalt karbonskatt skal fordeles ut til alle.
Jeg tenker heller at borgerlønn oppstår fra ingenting og at skatter og avgifter bare er destruksjon av penger.
Det som er viktig er at disse to pengestrømmene ikke skal være like store.
Man må alltid sørge for at borgerlønn-utbetalingene (pengeskapingen) er større enn skatte-innbetalingene (penge-destruksjonen),
for ellers blir det ikke en positiv inflasjon.
Moderat, stabil og forutsigbar inflasjon er bra! (Mellom 4% og 10% inflasjon i året, sånn omtrent.)

Men, tilbake til KAF og slikt. Idéen med KAF er å skattelegge det vi ønsker mindre av, energiproduksjon som øker CO2-nivået.
Jeg synes *all* energiproduksjon bør med, også den som *ikke* øker CO2-nivået.
En ulempe med fossil energi-produksjon er nemlig at mye av den blir borte i varme.
Så hvis man regner skatten som en øre-avgift pr. kWh primær-energi,
så vil en kunne sørge for at fossil-energi taper i forhold til fornybar energi.
Vi ønsker jo ikke at enhver fjelltopp skal ha en vindmølle på seg heller,
så hvorfor ikke systematisk skattlegge all energi?
Merk at avgiften ikke skal være et prosent-påslag av prisen, den skal være et (inflasjonsjustert) ørebeløp i forhold til
energi-innholdet i energikilden.

Denne avgiften kan ta helt over og erstatte moms også!
Hvis all energiproduksjon har avgift, vil jo produkter som trenger mye energi i produksjonen bli dyre.
Det vil gi et incentiv til å produsere varen på en så energieffektiv måte som mulig.
Kraftkrevende industri kan naturligvis ikke ha noe unntak fra energiavgiftsregelen.

Lønn og skatt
-------------

I tillegg til borgerlønn for alle, så vil høyst sannsynlig lønn for utført arbeid bestå.
Antakelig vil lønnsnivået synke slik at borgerlønn pluss ny lønn tilsvarer gammel lønn.
Om arbeidsinntekt skal skattes eller ikke vil jeg ikke ta stilling til her.

Historien om samfunnet
----------------------

Systemet som er beskrevet over vil løse endel problemstillinger:

- Ulikhetene kan reduseres (borgerlønn)
- Vi er ikke tvunget til å ha vekst (den altruistiske aktøren)
- Energi-behovet blir lavere (fordi vi ikke trenger vekst)
- Eldrebølgen er ikke noe problem (lavere behov for vekst frigjør hender til å bedrive omsorg)
- Gjeldsgraden går drastisk ned (fordi vi har en ny mekanisme for skaping av penger)
- Behovet for å utnytte jordens ressurser går ned
- Utslipp av klimagasser blir sterkt redusert

Eneste lille haken med opplegget er: Hvordan klare å fortelle folk at dette er mulig?
Det er her behovet for indoktrinering slår til.
Vi må prate et slikt system inn i vår bevissthet og sørge for at dagens
"naive fantasi" blir morgendagens virkelighet.

Det er naturligvis en del tekniske detaljer som pengeskaping, pengedestruksjon avgift pr. energienhet som må redegjøres for.
Samt sentralbankens rolle, obligasjoner, banksystemets rolle, delingen i økonomiske sektorer, osv.

Men det vanskeligste er nok dette med "gaveoverrekkelsen".
Er det rettferdig?
Vil vi føle oss forpliktet til å gi en ytelse tilbake?
Hvem er gaven fra?
Hvorfor skal vi destruere penger? Er det samfunnets betaling til naturen for de ressursene vi henter ut, liksom?
Er en forutsetning for at dette skal fungere at hele verden går over til denne samfunnsorganiseringen?

Svart svane
-----------

I økonomisk teori betyr begrepet
`Svart svane
<https://no.wikipedia.org/wiki/Svart_svane-teorien>`_
et paradigmeskifte, en ny metode eller tendens som er veldig lite sannsynlig
men som kan vise seg å ha stor betydning, enten i negativ eller positiv retning.
En samfunnsmodell bygd på prinsippene bak
`gaveøkonomi
<https://no.wikipedia.org/wiki/Gave%C3%B8konomi>`_
kan komme til å vise seg å være en positiv svart svane.
